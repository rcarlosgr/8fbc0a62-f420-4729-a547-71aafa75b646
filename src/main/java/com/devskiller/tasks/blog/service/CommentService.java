package com.devskiller.tasks.blog.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import com.devskiller.tasks.blog.model.Comment;
import com.devskiller.tasks.blog.repository.CommentRepository;
import com.devskiller.tasks.blog.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.model.dto.NewCommentDto;

@Service
public class CommentService {
	@Autowired
	private PostRepository postRepository;

	@Autowired
	private CommentRepository commentRepository;


	/**
	 * Returns a list of all comments for a blog post with passed id.
	 *
	 * @param postId id of the post
	 * @return list of comments sorted by creation date descending - most recent first
	 */
	public List<CommentDto> getCommentsForPost(String postId) {
		return commentRepository.findByPostIdOrderByCreationDateDesc(postId);
	}

	/**
	 * Creates a new comment
	 *
	 * @param postId id of the post
	 * @param newCommentDto data of new comment
	 * @return id of the created comment
	 *
	 * @throws IllegalArgumentException if postId is null or there is no blog post for passed postId
	 */
	public String addComment(String postId, NewCommentDto newCommentDto) {
		postRepository.findById(postId).orElseThrow(() -> new IllegalArgumentException());
		Comment comment = commentRepository.save(new Comment(UUID.randomUUID().toString(), newCommentDto.content(), newCommentDto.author(), postId, LocalDateTime.now()));
		return comment.id();
	}
}
